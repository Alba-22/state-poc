import { CustomState, StatesEnum } from "@/utils/custom_state";
import axios from "axios";
import React, { useEffect, useState } from "react";

interface Item {
  id: number;
  title: string;
  body: string;
}

const GetPage: React.FC = () => {
  const [item, setItem] = useState<CustomState<Item[]>>({
    state: StatesEnum.Initial,
  });

  useEffect(() => {
    makeRequest();
  }, []);

  const makeRequest = async () => {
    setItem({ state: StatesEnum.Loading });
    try {
      setTimeout(async () => {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/posts"
        );
        setItem({ state: StatesEnum.Success, data: response.data });
      }, 1000);
    } catch {
      setItem({ state: StatesEnum.Failed, message: "Ocorreu um erro" });
    }
  };

  if (item.state === StatesEnum.Loading) {
    return (
      <div className="h-screen w-screen flex flex-col justify-center items-center text-white">
        LOADING
      </div>
    );
  } else if (item.state == StatesEnum.Failed) {
    return (
      <div className="h-screen w-screen flex flex-col justify-center items-center text-white">
        {item.message}
      </div>
    );
  } else if (item.state == StatesEnum.Success) {
    return (
      <div className="bg-slate-500 mx-auto">
        {item.data.map((e) => {
          return (
            <div>
              {e.id} - {e.title}
            </div>
          );
        })}
      </div>
    );
  }

  return <></>;
};

export default GetPage;
