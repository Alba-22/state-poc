import { CustomState, StatesEnum } from "@/utils/custom_state";
import { notification } from "antd";
import axios from "axios";
import React, { use, useEffect, useState } from "react";

const FormPage: React.FC = () => {
  const [item, setItem] = useState<CustomState<null>>({
    state: StatesEnum.Initial,
  });

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const createItem = async (title: string, description: string) => {
    setItem({ state: StatesEnum.Loading });
    try {
      await axios.post("https://jsonplaceholder.typicode.com/posts", {
        title: title,
        body: description,
        userId: 1,
      });
      setItem({ state: StatesEnum.Success, data: null });
      notification.success({ message: "Item criado com sucesso!" });
    } catch {
      setItem({
        state: StatesEnum.Failed,
        message: "Ocorreu um erro ao criar um item",
      });
    }
  };

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        createItem(title, description);
      }}
    >
      <div className="flex flex-col px-96 py-8 justify-center bg-slate-500 h-screen w-screen gap-y-4">
        <input
          key="title"
          className="bg-slate-500 border-2 border-slate-400 h-12 rounded-md px-2"
          type="text"
          placeholder="Título"
          required
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          className="bg-slate-500 border-2 border-slate-400 h-12 rounded-md px-2"
          key="description"
          type="text"
          placeholder="Descrição"
          required
          onChange={(e) => setDescription(e.target.value)}
        />
        <input
          type="submit"
          className="bg-orange-400 px-4 py-2 rounded-md cursor-pointer"
          value={item.state === StatesEnum.Loading ? "Carregando" : "Enviar"}
          disabled={item.state === StatesEnum.Loading}
        ></input>
      </div>
    </form>
  );
};

export default FormPage;
