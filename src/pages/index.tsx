import Link from "next/link";

export default function Home() {
  return (
    <div className="flex flex-col justify-center items-center bg-slate-600 w-screen h-screen gap-4">
      <button onClick={() => {}} className="bg-orange-400 px-4 py-2 rounded-md">
        <Link href="/get">Página de GET</Link>
      </button>
      <button onClick={() => {}} className="bg-orange-400 px-4 py-2 rounded-md">
        <Link href="/form">Página de FORM</Link>
      </button>
    </div>
  );
}
