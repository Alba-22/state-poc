export const enum StatesEnum {
  Initial,
  Loading,
  Success,
  Failed,
}
export type InitialState = {
  state: StatesEnum.Initial;
};

export type LoadingState = {
  state: StatesEnum.Loading;
};

export type SuccessState<T> = {
  state: StatesEnum.Success;
  data: T;
};

export type FailedState = {
  state: StatesEnum.Failed;
  message: string;
};

export type CustomState<T> =
  | InitialState
  | LoadingState
  | SuccessState<T>
  | FailedState;
